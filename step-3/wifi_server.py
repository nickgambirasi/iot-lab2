import socket

import picar_4wd as fc

import re

HOST = "10.0.0.241" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

power_val = 20

size = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen()

while True:

    try:

        client, clientInfo = s.accept()

    except: 
        print("Closing socket")
        client.close()
        s.close()
        break

    print("server recv from: ", clientInfo)

    # receive message and send actions back to the host machine
    msg = client.recv(size).decode('utf-8').strip()
    print(msg)

    """
    We want to be able to do the following from the web service:
    
        - Move the car forward or backwards
        - Turn the car
        - Stop the car
        - Set the car speed

    """
    # edge case for when message isn't received
    if not msg:

        continue

    # use message data to turn the car
    if msg == '87': # forwards

        fc.forward(power_val)
        d = 'forward'
        t = 'no'
        

    elif msg == '83': # backwards

        fc.backward(power_val)
        d = 'backward'
        t = 'no'

    elif msg == '65': # left

        fc.turn_left(power_val)
        d = 'idle'
        t = 'left'

    elif msg == '68': # right 

        fc.turn_right(power_val)
        d = 'idle'
        t = 'right'

    elif msg == '81': # stop

        fc.stop()
        d = 'idle'
        t = 'no'
        break

    elif msg == '49': # decrease speed

        if power_val >= 10:

            power_val -= 10

    elif msg == '51':

        if power_val <= 90:

            power_val += 10

    else:

        d = 'idle'
        t = 'no'
        fc.stop()

    ret_msg = f'{d} {t} {power_val}'.encode('utf-8')
    client.sendall(ret_msg)


print("Closing socket")
client.close()
s.close()
        